# one page guide

My favorite Hugo boilerplate to generate one-page guide from markdown notes.

The page design is entirely rely to [Bulma][bulmacss] and [bulmaswatch][bulmaswatch] theme.

## usage

Install `git` and `hugo`.

Then:

```sh
$ git clone https://bitbucket.org/lettenj61/one-page-guides.git <yoursitename>
$ cd <yoursitename>
$ hugo new guides/blah-blah.md
$ hugo server -D
```

Go to `http://localhost:1313/guides/blah-blah/` to see rendered page.

## license

This template is licensed under CC0.

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/deed)

[bulmacss]:https://bulma.io/
[bulmaswatch]:https://jenil.github.io/bulmaswatch/
